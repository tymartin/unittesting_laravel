<?php

namespace Tests\Unit;

//use PHPUnit\Framework\TestCase;
use Tests\TestCase; 
use Illuminate\Foundation\Testing\RefreshDatabase; 
use App\Todo; 

class TodoTest extends TestCase
{
    use RefreshDatabase;
    /**
     * A basic unit test example.
     *
     * @return void
     */
    public function testExample()
    {
        $this->assertTrue(true);
    }

    public function testAddTodo()
    {
        $this->withoutExceptionHandling(); 

    	$response = $this->post('/todos', [
    		"task" => "Task Title",
    		"description" => "this is a task description entry"
    	]); 

        $response->assertOk(); 
        $this->assertCount(1, Todo::all()); 
    }

    public function testAddTaskFieldRequired()
    {
        // $this->withoutExceptionHandling();

        $response = $this->post('/todos', [
            'description' => "task description"
        ]);

        $response->assertSessionHasErrors('task');
    }
}
